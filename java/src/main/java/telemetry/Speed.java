package telemetry;

public class Speed {
	
	public Speed(Speed currentSpeed) {
		this.pieceIndex = currentSpeed.pieceIndex;
		this.inPieceDistance = currentSpeed.inPieceDistance;
		this.speedPerTick = currentSpeed.speedPerTick;
		this.tickNumber = currentSpeed.tickNumber;
	}

	public Speed() {
		
	}

	@Override
	public String toString() {
		return "Speed [pieceIndex=" + pieceIndex + ", inPieceDistance="
				+ inPieceDistance + ", speedPerTick=" + speedPerTick + "]";
	}
	
	int pieceIndex = 0;
	Double inPieceDistance = 0.0d;
	Double speedPerTick = 0.0d;
	double tickNumber = 0;
	public Double laneLength = 0.0;
	
	public Double getSpeedPerTick() {
		return speedPerTick;
	}
}
