package telemetry;

public class SpeedControll {
	
	Speed currentSpeed = null;
	
	double oldAngle = -90.0d;
	double angleGrowFactor = 0.0d;
	
	public void calculateCurrentSpeed(int trackIndex, Double inPieceDistance, Double tickNumber, double laneLength) {
		if (tickNumber == 0) return;
		if (currentSpeed != null) {
			
			if (trackIndex != currentSpeed.pieceIndex) {
				Double distanceTravelled = currentSpeed.laneLength - currentSpeed.inPieceDistance + inPieceDistance;
				double tickDelta = tickNumber - currentSpeed.tickNumber;
				
				currentSpeed.speedPerTick = (distanceTravelled / tickDelta);
				
				currentSpeed.pieceIndex = trackIndex;
				currentSpeed.inPieceDistance = inPieceDistance;
				currentSpeed.tickNumber = tickNumber;
				currentSpeed.laneLength = laneLength;
			}
			// same piece...
			else {
				Double distanceTravelled = inPieceDistance - currentSpeed.inPieceDistance;
				double tickDelta = tickNumber - currentSpeed.tickNumber;
				
				currentSpeed.speedPerTick = (distanceTravelled / tickDelta);
				
				currentSpeed.pieceIndex = trackIndex;
				currentSpeed.inPieceDistance = inPieceDistance;
				currentSpeed.tickNumber = tickNumber;
				currentSpeed.laneLength = laneLength;
			}
			
			
		}
		// first piece of track...
		else {
			currentSpeed = new Speed();
		}
	}
	

	public Speed getCurrentSpeed() {
		return currentSpeed;
	}


	public void calculateAngleGrowFactor(Double angle) {
		if (oldAngle != -90) {
			angleGrowFactor = Math.abs(angle) - Math.abs(oldAngle);
		}
		oldAngle = angle;
	}


	public double getAngleGrowFactor() {
		return angleGrowFactor;
	}


	public void resetAll() {
		currentSpeed = null;
		oldAngle = -90.0d;
	}

}
