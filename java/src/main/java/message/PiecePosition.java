package message;

public class PiecePosition {
	public Integer pieceIndex;
	public Double inPieceDistance;
	public LaneInformation lane;
	
	public Double getInPieceDistance() {
		return inPieceDistance;
	}

	public Integer getPieceIndex() {
		return pieceIndex;
	}

	public PiecePosition(Integer pieceIndex, Double inPieceDistance, LaneInformation lane) {
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
	}
}