package message;

import noobbot.Race;

public class RaceWrapper {
	public final Race race;
	
	public RaceWrapper(final Race race) {
		this.race = race;
	}
	
	public Race getRace() {
		return race;
	}
}
