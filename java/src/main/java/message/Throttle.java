package message;


public class Throttle extends SendMsg {
    private double value;
    private Object gameTick;

    public Throttle(double value) {
        this.value = value;
    }

    public Throttle(Double speed, Object gameTick) {
    	this.value = speed;
    	this.gameTick = gameTick;
	}

	@Override
    protected Object msgData() {
        return value;
    }
	
	@Override
	protected Object msgTick() {
		return gameTick;
	}

    @Override
    protected String msgType() {
        return "throttle";
    }

	@Override
	public String toString() {
		return "Throttle [value=" + value + ", gameTick=" + gameTick + "]";
	}
    
}