package message;

public class CarPosition {
	public CarId id;
	public Double angle;
	public PiecePosition piecePosition;
	
	
	public CarPosition(CarId id, Double angle, PiecePosition piecePosition, LaneInformation lane) {
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}
}