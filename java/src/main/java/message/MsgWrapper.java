package message;


public class MsgWrapper {
    public final String msgType;
    public final Object gameTick;
    public final Object data;

    MsgWrapper(final String msgType, final Object data, final Object gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }
    
    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = null;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.msgTick());
    }
}