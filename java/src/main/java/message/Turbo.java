package message;

public class Turbo  extends SendMsg{
    private String data;

    public Turbo(String data) {
        this.data = data;
    }

    @Override
    protected Object msgData() {
        return data;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
