package message;

public class SwitchLane extends SendMsg {
    private String data;

    public SwitchLane(String data) {
        this.data = data;
    }

    @Override
    protected Object msgData() {
        return data;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

}
