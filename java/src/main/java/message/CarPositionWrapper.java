package message;

import java.util.HashMap;
import java.util.List;

public class CarPositionWrapper {
	private final List<CarPosition> data;
	private HashMap<String, CarPosition> positions;
	
	
	public CarPositionWrapper(final List<CarPosition> carPosition) {
		this.data = carPosition;
	}
	
	public List<CarPosition> getCarPosition() {
		return data;
	}

	public CarPosition getCarPositionForBot(String string) {
		if (positions == null) {
			positions = new HashMap<String, CarPosition>();
			
			for (CarPosition position: data) {
				positions.put(position.id.name, position);
			}
		}
		
		return positions.get(string);
	}
}
