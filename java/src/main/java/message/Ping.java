package message;


public class Ping extends SendMsg {
	
	private Object gameTick;
	
    public Ping(Object gameTick) {
		this.gameTick = gameTick;
	}
    
    public Ping() {
	}

	@Override
    protected Object msgTick() {
    	return gameTick;
    }
    
	@Override
    protected String msgType() {
        return "ping";
    }
}