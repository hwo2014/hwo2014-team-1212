package noobbot;

import java.util.Hashtable;
import java.util.Map.Entry;

public class TrackSector {
	int startIndex;
	int endIndex;
	Hashtable<Lane, Double> laneLengths = new Hashtable<Lane, Double>();
	
	public TrackSector(int startIndex, int endIndex) {
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}

	public Lane getShortestLane() {
		Entry<Lane, Double> shortest = null;
		for (Entry<Lane, Double> entry: laneLengths.entrySet()) {
			if (shortest == null || shortest.getValue() > entry.getValue())  {
				shortest = entry;
			}
		}
		return shortest.getKey();
	}
	
	public void setLaneLength(Lane lane, Double length) {
		if (laneLengths == null) {
			laneLengths = new Hashtable<Lane, Double>();
		}
		laneLengths.put(lane, length);
	}

	@Override
	public String toString() {
		return "TrackSector [startIndex=" + startIndex + ", endIndex="
				+ endIndex + ", laneLengths=" + laneLengths + "]";
	}
	
	
}
