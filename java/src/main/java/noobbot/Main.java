package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import message.Join;
import message.MsgWrapper;
import message.SendMsg;

import com.google.gson.Gson;

/*
 * Testing, testing... 1, 2, 3
 * */


public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        
        RaceControll controll = new RaceControll(gson);
        
        send(join);

        while((line = reader.readLine()) != null) {
            
            SendMsg message = controll.handleMessage(line);
            
            if (message != null) {
            	send(message);
            }
            
        }
    }

    private void send(final SendMsg msg) {
//    	System.out.println("Sending: " + msg);
        writer.println(msg.toJson());
        writer.flush();
    }
}