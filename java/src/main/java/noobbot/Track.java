package noobbot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Track {
	
	public String id;
	public String name;
	public List<TrackPiece> pieces;
	public List<Lane> lanes;
	List<TrackSector> sectors;
	
	public HashMap<Integer, TrackPiece> pieceMap;

	public Track(final String id, final String name, final List<TrackPiece> pieces, final List<Lane> lanes) {
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
	}
	
	public void putPiecesToMap() {
		pieceMap = new HashMap<Integer, TrackPiece>();
		int i = 0;
		for (TrackPiece p: pieces) {
			pieceMap.put(i, p);
			i++;
		}
	}
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Collection<TrackPiece> getPiecesList() {
		return pieces;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPiecesList(List<TrackPiece> pieces) {
		this.pieces = pieces;
	}
	
	public HashMap<Integer, TrackPiece> getPieceMap() {
		return pieceMap;
	}
	
	public List<Lane> getLanes() {
		return lanes;
	}

	public void setLanes(List<Lane> lanes) {
		this.lanes = lanes;
	}

	public void calculateTrackLength() {
		for (TrackPiece piece: pieces) {
			piece.calculateLengths(lanes);
		}
	}
	
	int firstIndex = 0;
	int startIndex = 0;
	int endIndex = 0;
	
	public void fetchSectors() {		
		for (Entry<Integer, TrackPiece> entry: pieceMap.entrySet()) {
			TrackPiece piece = entry.getValue();
			
			if (piece.hasSwitch) {
				if (startIndex == 0) {
					firstIndex = entry.getKey();
					startIndex = entry.getKey();
				}
				else if (startIndex > 0 || endIndex == 0) {
					endIndex = entry.getKey() - 1;
					createSector(startIndex, endIndex);
					
				}
			}
		}
		
		createSector(startIndex, firstIndex);
	}
	
	private void createSector(int startIndex, int endIndex) {
		if (sectors == null) {
			sectors = new ArrayList<TrackSector>();
		}
		
		TrackSector sector = new TrackSector(startIndex, endIndex);
		sectors.add(sector);
		
		this.startIndex = endIndex + 1;
		this.endIndex = 0;
	}
	
	public void calculateSectorLengths() {
		for (TrackSector sector: sectors) {
			// over start line
			if (sector.startIndex > sector.endIndex) {
				for (Lane lane: lanes) {
					double laneLength = 0.0d;
					for (int i = sector.startIndex; i < pieceMap.size(); i++) {
						laneLength += pieceMap.get(i).getLengthForLane(lane);
					}
					for (int i = 0; i < sector.endIndex; i++) {
						laneLength += pieceMap.get(i).getLengthForLane(lane);
					}
					sector.laneLengths.put(lane, laneLength);
				}
			}
			else {
				for (Lane lane: lanes) {
					double laneLength = 0.0d;
					for (int i = sector.startIndex; i < sector.endIndex; i++) {
						laneLength += pieceMap.get(i).getLengthForLane(lane);
					}
					sector.laneLengths.put(lane, laneLength);
				}
			}
		}
	}

	public void printLaneLengths() {
		for (Lane lane: lanes) {
			double laneLength = 0.0d;
			for (Entry<Integer,TrackPiece> entry: pieceMap.entrySet()) {
				TrackPiece piece = entry.getValue();
				laneLength += piece.getLengthForLane(lane);
			}
			System.out.println("Lane " + lane.index + " length: " + laneLength);
		}
	}
	
	public void printSectors() {
		for (TrackSector sector: sectors) {
			System.out.println(sector);
		}
	}

	public TrackSector getSectorForPieceIndex(int index) {
		for (TrackSector sector: sectors) {
			if (sector.startIndex == index)
				return sector;
		}
		return null;
	}

	public Lane getLaneForIndex(int startLaneIndex) {
		for (Lane lane: lanes) {
			if (lane.index == startLaneIndex)
				return lane;
		}
		return null;
	}

	public double calculateStraightLength(Integer pieceIndex,
			Double inPieceDistance) {
		double straightLength = 0.0d;
		
		while (pieces.get(pieceIndex).length != null) {
			straightLength += pieces.get(pieceIndex).length;
			pieceIndex++;			
			if (pieceIndex > pieces.size() - 1) {
				pieceIndex = 0;
			}
		}
		
		return straightLength;
	}

	public double calculateRemainingStraightLength(Integer pieceIndex,
			Double inPieceDistance) {
		double straightLength = 0.0d;
	
		straightLength = pieces.get(pieceIndex).length - inPieceDistance;
		
		pieceIndex++;
		if (pieceIndex > pieces.size() - 1) {
			pieceIndex = 0;
		}
		
		while (pieces.get(pieceIndex).length != null) {
			straightLength += pieces.get(pieceIndex).length;
			pieceIndex++;			
			if (pieceIndex > pieces.size() - 1) {
				pieceIndex = 0;
			}
		}
		
		return straightLength;
	}
}
