package noobbot;

import telemetry.SpeedControll;

import com.google.gson.Gson;

import message.CarPositionWrapper;
import message.MsgWrapper;
import message.Ping;
import message.RaceWrapper;
import message.SendMsg;
import message.SwitchLane;
import message.Throttle;
import message.Turbo;

public class RaceControll {
	Race race;
	Gson gson;
	SpeedControll sc = new SpeedControll();
	private boolean switchHandled = false;
	int switchIndex;
	private boolean raceStarted = false;
	
	private int turboCharges = 0;
	private int turboDuration = 0;
	private boolean turboUsedRecently = false;
	
	public RaceControll(Gson gson) {
		this.gson = gson;
	}

	public SendMsg handleMessage(String line) {
		
		final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
		
//		System.out.println(msgFromServer.msgType + ": " + msgFromServer.data + " @ tick: " + msgFromServer.gameTick);
		
		if (msgFromServer.msgType.equals("gameInit")) {			
			RaceWrapper raceWrapper = gson.fromJson(msgFromServer.data.toString(), RaceWrapper.class);
			race = raceWrapper.getRace();
			race.track.calculateTrackLength();
			race.track.putPiecesToMap();
			race.track.fetchSectors();
			race.track.calculateSectorLengths();
			race.track.printLaneLengths();
			race.track.printSectors();
			return new Ping();
        }        
        else if (msgFromServer.msgType.equals("carPositions")) {
        	
        	// starting positions
        	if (raceStarted == false || msgFromServer.gameTick == null) {
        		return new Ping();
        	}
        	
        	Double slideCompensator = 0.0d;
        	CarPositionWrapper carPositionWrapper = gson.fromJson(line, CarPositionWrapper.class);
        	
        	int thisIndex = carPositionWrapper.getCarPositionForBot("Smarde").piecePosition.getPieceIndex();
        	
        	sc.calculateCurrentSpeed(thisIndex, carPositionWrapper.getCarPositionForBot("Smarde").piecePosition.getInPieceDistance(),(Double) (msgFromServer.gameTick == null ? 0 : msgFromServer.gameTick), race.track.pieceMap.get(thisIndex).getLengthForLane(race.track.getLaneForIndex(carPositionWrapper.getCarPositionForBot("Smarde").piecePosition.lane.startLaneIndex)));
        	sc.calculateAngleGrowFactor(carPositionWrapper.getCarPositionForBot("Smarde").angle);
        	
        	if ((sc.getAngleGrowFactor() > 3.2 && (Math.abs(carPositionWrapper.getCarPositionForBot("Smarde").angle)) > 10) || (Math.abs(carPositionWrapper.getCarPositionForBot("Smarde").angle) > 43 && sc.getAngleGrowFactor() > 0)) {
        		slideCompensator = -0.65d;
//        		System.out.println("SLIDE!!! Foot off from the pedal!");
        	}
        	
        	int  nextIndex = thisIndex + 1;
        	Double speed = 0.0d;
        	if (nextIndex == race.track.getPieceMap().size())
        		nextIndex = 0;
        	
        	int nextNextIndex = nextIndex + 1;
        	
        	if (nextNextIndex == race.track.getPieceMap().size())
        		nextNextIndex = 0;
        	        	
        	// check for long turbo straight, simple one for the moment...
        	if (race.track.pieceMap.get(thisIndex).length != null && race.track.calculateStraightLength(thisIndex, carPositionWrapper.getCarPositionForBot("Smarde").piecePosition.getInPieceDistance()) > 600 && turboCharges > 0 && !turboUsedRecently) {
        		turboUsedRecently = true;
        		turboDuration = 0;
//        		System.out.println("TUUUUURBOOOOO!");
        		turboCharges--;
        		return new Turbo("HIT IT!");
        	}
        	
        	boolean brakeFromTurbo = false;
        	if (turboUsedRecently && turboDuration < 30) {
        		turboDuration++;
        		double laneLength = race.track.calculateRemainingStraightLength(thisIndex, carPositionWrapper.getCarPositionForBot("Smarde").piecePosition.getInPieceDistance());
        		double speedPerTick = sc.getCurrentSpeed().getSpeedPerTick();
        		
//        		System.out.println("Turbo used!  length: " + laneLength + " speed: " + speedPerTick);
        		
        		if (laneLength /  speedPerTick < 57.1d) {
//        			System.out.println("Brake for it!  length/speed: " + laneLength /  speedPerTick + " turboDuration: " + turboDuration);
        			brakeFromTurbo = true;
        		}
        	}
        	
        	// switch lanes
        	if (race.track.pieceMap.get(nextIndex).hasSwitch && !switchHandled && nextIndex != 35) {
        		TrackSector sector = race.track.getSectorForPieceIndex(nextIndex);
        		if (sector != null) {
        			int currentLaneIndex = carPositionWrapper.getCarPositionForBot("Smarde").piecePosition.lane.startLaneIndex;
        			int shortestLaneIndex = sector.getShortestLane().index;
        			switchHandled = true;
        			switchIndex = thisIndex;
        			
        			if (currentLaneIndex != shortestLaneIndex) {
//        				System.out.println("Switch!");
        				if (currentLaneIndex < shortestLaneIndex){
        					return new SwitchLane("Right");
        				}
        				else {
        					return new SwitchLane("Left");
        				}
        			}
        		}
        	}
        	
        	if (switchIndex != thisIndex) {
        		switchHandled = false;
        	}
        	
        	// start of race(?)
        	if (msgFromServer.gameTick == null) {
        		speed = 1.00;
        	}
        	
        	if (brakeFromTurbo) {
        		speed = 0.0;
        	}
        	else if (turboUsedRecently && turboDuration <= 29) {
        		speed = 1.0d;
        	}
        	// long straight, small cornes don't count.
        	else if (race.track.pieceMap.get(nextNextIndex).length != null && Math.abs(carPositionWrapper.getCarPositionForBot("Smarde").angle) < 40 && sc.getAngleGrowFactor() <= 3.0 && sc.getCurrentSpeed().getSpeedPerTick() < 10.0) {
//        		System.out.println("long straight coming, need more speed");
        		speed = 1.00;
        	}
        	// corner coming
        	else {
        		if (turboDuration >= 29) {
        			turboUsedRecently = false;
        		}
    			// going too fast, full brakes;
        		if (sc.getCurrentSpeed().getSpeedPerTick() > 8.1) {
    				speed = 0.0;
        		}
    			else if (sc.getCurrentSpeed().getSpeedPerTick() < 6.6 && slideCompensator == 0.0) {
    				if (Math.abs(carPositionWrapper.getCarPositionForBot("Smarde").angle) < 50 && sc.getAngleGrowFactor() < 0.0) {
//    					System.out.println("slow on corner, throttle up!");
    					speed = 1.0;
    				}
    				else 
    					speed = 0.7;
    			}
        		else {
        			speed = 0.65;
        		}
        	}
        	
        	speed += slideCompensator;
        	if (speed < 0)
        		speed = 0.0;
        	        	
        	return new Throttle(speed, msgFromServer.gameTick);
        	
        } else if (msgFromServer.msgType.equals("join")) {
            System.out.println("Joined");
        } else if (msgFromServer.msgType.equals("gameEnd")) {
            System.out.println("Race end");
            clearStuff();
        } else if (msgFromServer.msgType.equals("gameStart")) {
            System.out.println("Race start");
            raceStarted  = true;
            return new Ping(msgFromServer.gameTick);
        } else if (msgFromServer.msgType.equals("turboAvailable")) {
        	turboCharges++;
        	return new Ping(msgFromServer.gameTick);
        } else {
            return new Ping();
        }
		return null;
	}

	private void clearStuff() {
		sc.resetAll();
		raceStarted = false;
		turboUsedRecently = false;
	}

}
