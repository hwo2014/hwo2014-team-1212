package noobbot;

public class Lane {

	int index;
	int distanceFromCenter;
	
	@Override
	public String toString() {
		return "Lane [index=" + index + ", distanceFromCenter="
				+ distanceFromCenter + "]";
	}
}
