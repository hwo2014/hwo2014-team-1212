package noobbot;

import java.util.HashMap;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TrackPiece {
	public Double length;
	@SerializedName("switch")
	public boolean hasSwitch;
	public Double radius;
	public Double angle;
	
	HashMap<Lane, Double> laneLengths;
	
	public void calculateLengths(List<Lane> lanes) {
		
		laneLengths = new HashMap<Lane, Double>();
		
		for (Lane lane: lanes) {
			if (length != null) {
				laneLengths.put(lane, length);
			}
			// Corners
			else {
				int deltaMultiplier = 1;
				double laneLength = 0.0d;
				
				// right bend
				if (angle > 0) {
					deltaMultiplier = -1;
				}
				
				laneLength = (Math.abs(angle) / 360) * (2 * Math.PI * (radius + (deltaMultiplier * lane.distanceFromCenter)));		
				
				laneLengths.put(lane, laneLength);
			}
		}
	}

	public double getLengthForLane(Lane lane) {
		return laneLengths.get(lane);
	}
}
